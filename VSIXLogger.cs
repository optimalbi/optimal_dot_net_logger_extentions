﻿using System;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace Optimal.DotNetLogger{
    public class VsixLogger : Logger{
        private readonly IVsOutputWindowPane collabPane;
        private readonly FileLogger fileLogger;

        public VsixLogger(){
            fileLogger = new FileLogger();
            IVsOutputWindowPane generalPane;
            IVsOutputWindowPane debugPane;
            IVsOutputWindow outWindow = Package.GetGlobalService(typeof (SVsOutputWindow)) as IVsOutputWindow;

            Guid generalPaneGuid = VSConstants.GUID_OutWindowGeneralPane;
            // P.S. There's also the GUID_OutWindowDebugPane available.
            Guid debugPaneGuid = VSConstants.GUID_OutWindowDebugPane;
            // P.S. There's also the GUID_OutWindowDebugPane available.
            Guid collabPaneGuid = new Guid("0F44E2D1-F5FA-4d2d-AB30-22BE8ECD9789");
            outWindow?.CreatePane(ref collabPaneGuid, "Gnimag Collab", 1, 1);

            outWindow?.GetPane(ref generalPaneGuid, out generalPane);
            outWindow?.GetPane(ref debugPaneGuid, out debugPane);
            outWindow?.GetPane(ref collabPaneGuid, out collabPane);

            this.Debug("We are logging to " + fileLogger.LogPath());
        }

        protected override void Write(LogLevel level, string message){
            try {
                collabPane.OutputStringThreadSafe(message + "\r\n");
                collabPane.Activate();
            } catch (Exception e) {
                fileLogger.Error("Failed to write to the Log the following text" + message + "\nReason for log failer: " + e.ToString());
            }
        }

    }
}