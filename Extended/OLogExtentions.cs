﻿// Timothy Gray
// 0121
// 2016012112:53 PM

using System;

namespace Optimal.DotNetLogger.Extended{
    public static class OLogExtentions{
        public enum ExtendedLoggers {
            Vsix
        }

        public static void SwitchLogger(ExtendedLoggers logger) {
            switch (logger) {
                case ExtendedLoggers.Vsix:
                    OLog.Logger = new VsixLogger();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(logger), logger, null);
            }
        }
    }
}