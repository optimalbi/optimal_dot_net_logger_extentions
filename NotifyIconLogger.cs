﻿// Timothy TAG. Gray
// 0107
// 2016010711:16 PM

using System;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Optimal.DotNetLogger {
    public class NotifyIconLogger : Logger {
        private readonly NotifyIcon notifyIcon;
        public int Timeout { get; set; } = 4000;

        public NotifyIconLogger() {
            notifyIcon = new NotifyIcon {
                Visible = false,
                Icon = new Icon("Resources/LoginPackage.ico")
            };
        }

        public void CloseIcon() {
            notifyIcon.Visible = false;
        }

        public void BalloonTip(string message, ToolTipIcon ico) {
            notifyIcon.Visible = true;
            notifyIcon.ShowBalloonTip(Timeout, "Gnimag Collab", message, ico);
            notifyIcon.Visible = false;
        }

        protected override void Write(LogLevel level, string message) {
            switch (level) {
                case LogLevel.Error:
                    this.BalloonTip(message,ToolTipIcon.Error);
                    break;
                case LogLevel.Warn:
                    this.BalloonTip(message, ToolTipIcon.Warning);
                    break;
                case LogLevel.Info:
                    this.BalloonTip(message, ToolTipIcon.Info);
                    break;
                case LogLevel.Debug:
                    this.BalloonTip(message, ToolTipIcon.None);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }
}